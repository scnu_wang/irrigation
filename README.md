# irrigation

#### 项目介绍
灌水系统
自动登录京东众筹，并对社区文章进行评论

#### 软件架构
- Python 3.6.6
- Django 2.1.2
- Selenium 3.14.1

#### 安装教程

1. 安装Python、Django、Selenium
2. 执行下面命令，进行数据迁移
```python
python manage.py makemigrations
python manage.py migrate
```


#### 使用说明

1. localhost:8000：打开默认的用户列表，点击对应账户后面的登录按钮即可
2. localhost:8000/admin：默认的后台管理界面


#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)