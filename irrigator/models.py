from django.db import models

class User(models.Model):
    nickname = models.CharField('昵称',max_length=64,null=True,blank=True)
    username = models.CharField('用户名',max_length=64)
    password = models.CharField('密码',max_length=64)
    # 1: 正常 2: 异常
    status = models.BooleanField('用户状态',default=True)
