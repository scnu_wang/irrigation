from django.apps import AppConfig


class IrrigatorConfig(AppConfig):
    name = 'irrigator'
